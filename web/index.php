<?php

ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\RedirectResponse;

$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);

$app = new Silex\Application();

$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => 'views'));
$app->register(new Silex\Provider\DoctrineServiceProvider(), array('db.options' => $config['database']));

$app->get('/', function () use ($app) {
    return $app['twig']->render('manual.twig', array());
});

$app->get('/get-news', function () use ($app) {

    if (isset($_GET['f']) && $_GET['f'] = 1) {

        $domain = 'undefined';

        if (isset($_SERVER['HTTP_REFERER'])) {
            $domain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        }

        $isThereSql = $app['db']->fetchColumn(
            "SELECT count(*) FROM statistics WHERE statistics.domain = ? AND date = CURDATE()",
            array($domain),
            0
        );

        if ($isThereSql != 0) {
            $app['db']->executeUpdate(
                "UPDATE statistics SET statistics.views = statistics.views + 1  WHERE domain = ? AND date = CURDATE()",
                array($domain)
            );
        } else {
            $app['db']->executeUpdate(
                "INSERT INTO `statistics` (`id`, `domain`, `views`, `clicks`, `date`) VALUES (NULL, ?, '1', '0', CURDATE())",
                array($domain)
            );
        }
    }

    $lastNews = $app['db']->fetchAll('SELECT * FROM news ORDER BY id DESC LIMIT 5');

    header('Access-Control-Allow-Origin: *');

    return $app['twig']->render('news.twig', array('news' => $lastNews));
});

$app->get('/click/{hash}', function ($hash) use ($app) {

    $domain = 'undefined';

    if (isset($_SERVER['HTTP_REFERER'])) {
        $domain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
    }

    $isThereSql = $app['db']->fetchColumn(
        "SELECT count(*) FROM statistics WHERE statistics.domain = ? AND date = CURDATE()",
        array($domain),
        0
    );

    if ($isThereSql != 0) {

        $app['db']->executeUpdate(
            "UPDATE statistics SET statistics.clicks = statistics.clicks + 1  WHERE domain = ? AND date = CURDATE()",
            array($domain)
        );

    } else {
        $app['db']->executeUpdate(
            "INSERT INTO `statistics` (`id`, `domain`, `views`, `clicks`, `date`) VALUES (NULL, ?, '0', '1', CURDATE())",
            array($domain)
        );
    }

    $news = $app['db']->fetchAll('SELECT * FROM news WHERE hash = ?', array($hash));

    return new RedirectResponse($news[0]['url']);
})
    ->bind('click');

$app->get('/statistics', function () use ($app) {

    $table = $app['db']->fetchAll('SELECT * FROM `statistics` ORDER BY date DESC, domain');
    return $app['twig']->render('statistics.twig', array('table' => $table));
});

$app->run();