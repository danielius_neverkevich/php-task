var initFeed = function (newsFeedUrl) {

    function updateFeed(newsFeedUrl) {
        var r = new XMLHttpRequest();
        r.open("GET", newsFeedUrl, true);
        r.onreadystatechange = function () {
            if (r.readyState != 4 || r.status != 200) return;
            document.getElementById('news-widget').innerHTML = r.responseText;
        };
        r.send();
    }

    updateFeed(newsFeedUrl + '?f=1');

    setInterval(function () {updateFeed(newsFeedUrl);}, 60000);
};

