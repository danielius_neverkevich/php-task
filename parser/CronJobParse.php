<?php

require_once 'Parser.php';

$config = json_decode(file_get_contents( '../web/config.json'), true);
$url = "https://bg.pokernews.com/news/%D0%BF%D0%BE%D0%BA%D0%B5%D1%80-%D0%B7%D0%B0%D0%BA%D0%BE%D0%BD-%D0%BE%D0%B1%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%BE/";

$options = array(
    'domain' => 'https://bg.pokernews.com',
    'page' => array(
        'title' => 'h1:first',
        'img' => '.leadPhoto',
        'text' => '#container p:first'
    ),
    'news' => array(
        'news_link' => '.title'
    )
);

$parser = new Parser($url, $options);
$parser->startParse();

$news = array_reverse($parser->getResult());

if (count($news) > 0) {

    $hashes = array();

    $conn = new mysqli(
        $config['database']['dbhost'],
        $config['database']['user'],
        $config['database']['password'],
        $config['database']['dbname']
    );

    $conn->set_charset($config['database']['charset']);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $query = $conn->query('SELECT hash FROM `news`');

    while ($row = $query->fetch_array()) {
        $hashes[] = $row['hash'];
    }

    foreach ($news as $item) {
        if (in_array($item['hash'], $hashes)) {
            continue;
        }

        foreach ($item as $key => $value) {
            $item[$key] = $conn->real_escape_string($value);
        }

        $sql = "
        INSERT INTO `news`
            (`id`,
             `title`,
             `text`,
             `img_url`,
             `add_date`,
             `url`,
             `hash`)
        VALUES
            (NULL,
             '" . $item['title'] . "',
             '" . $item['text'] . "',
             '" . $item['img'] . "',
             NOW(),
             '" . $item['url'] . "',
             '" . $item['hash'] . "')
        ";

        if ($conn->query($sql)) {
            echo "Add new Item" . PHP_EOL;
        }
    }
}

