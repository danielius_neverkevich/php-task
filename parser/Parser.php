<?php

include '../vendor/phpQuery-master/phpQuery/phpQuery.php';

class Parser
{
    private $url = null;
    private $news = array();
    private $options = array();

    function __construct($url, $options)
    {
        $this->url = $url;
        $this->options = $options;
    }

    public function startParse()
    {
        $urls = $this->parseIndex($this->url);

        if (!empty($urls)) {
            foreach ($urls as $url) {
                $this->parsePage($url);
            }
        }
    }

    private function parseIndex($url)
    {
        $urls = array();
        $html = $this->getPage($url);

        $doc = phpQuery::newDocument($html);
        phpQuery::selectDocument($doc);

        if (isset($this->options['news']['news_link']) && !empty($html)) {

            $selectedUrls = $doc->find($this->options['news']['news_link']);

            foreach ($selectedUrls as $item) {

                $tempUrl = pq($item)->attr('href');

                if (!empty($tempUrl)) {
                    $urls[] = $this->options['domain'] . $tempUrl;
                }
            }
        }

        return $urls;
    }

    private function parsePage($url)
    {
        $temp = array();

        $html = $this->getPage($url);

        if (!empty($html)) {
            $temp['url'] = $url;
            $temp['hash'] = sha1($temp['url']);
        } else {
            return;
        }

        $doc = phpQuery::newDocument($html);
        phpQuery::selectDocument($doc);

        if (isset($this->options['page']['title'])) {
            $temp['title'] = $doc->find($this->options['page']['title'])->text();
        }

        if (isset($this->options['page']['img'])) {
            $temp['img'] = $doc->find($this->options['page']['img'])->attr('src');
        }

        if (isset($this->options['page']['text'])) {
            $temp['text'] = $doc->find($this->options['page']['text'])->text();
        }

        unset($doc);

        $this->news[] = $temp;
    }

    private function getPage($url)
    {
        return file_get_contents($url);
    }

    public function getResult()
    {
        return $this->news;
    }

    public function printResult()
    {
        print_r($this->news);
    }
}
